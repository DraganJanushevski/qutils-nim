import ../COMMON/HEADER_BSPFILE
import ../COMMON/BSPFILE
import ../COMMON/MATHLIBH
import ../COMMON/ptrmath


type
  Tnode = object
    nodetype: int32
    normal: Vec3
    dist: float32
    children: array[2, int32]
    pad: int32

  Tracestack = object
    backpt: Vec3
    side: int32
    node: int32

const ON_EPSILON = 0.1'f32
var tnodes: array[MAX_MAP_NODES, Tnode]

proc MakeTnode(nodenum: int32) =
  ## Converts the disk node structure
  ## into the efficient tracing structure
  let node =  dnodes[nodenum]
  let plane = dplanes[node.planenum]

  var t: Tnode
  t.nodetype = plane.planetype
  t.normal = plane.normal
  t.dist = plane.dist

  for i in 0..1:
    if node.children[i] < 0:
      t.children[i] = dleafs[-node.children[i] - 1].contents
      tnodes[nodenum] = t
    else:
      t.children[i] = node.children[i]
      tnodes[nodenum] = t
      MakeTnode(node.children[i])

proc MakeTnodes*(bm: ptr dmodel_t) =
  ## Loads the node structure out of a .bsp file
  ## to be used for light occlusion
  MakeTnode(0)


proc TestLine*(start: Vec3; stop: Vec3): bool =
  ## LINE TRACING:
  ## The major lighting operation is a point to point visibility test,
  ## performed recursive subdivision of the line by the BSP tree.
  var node: int32
  var side: int32
  var front, back: float32
  var frontx, fronty, frontz: float32
  var backx, backy, backz: float32
  var tracestack: array[64, Tracestack]
  var tstack_p: ptr Tracestack
  var tnode: ptr Tnode

  frontx = start[0]
  fronty = start[1]
  frontz = start[2]
  backx = stop[0]
  backy = stop[1]
  backz = stop[2]
  tstack_p = tracestack[0].addr
  node = 0

  while true:
    while node < 0 and node != CONTENTS_SOLID:

      tstack_p -= 1
      if tstack_p < tracestack[0].addr:
        return true

      node = tstack_p.node
      ##  set the hit point for this plane
      frontx = backx
      fronty = backy
      frontz = backz
      ##  go down the back side
      backx = tstack_p.backpt[0]
      backy = tstack_p.backpt[1]
      backz = tstack_p.backpt[2]

      let child = (not tstack_p.side) + 2
      node = tnodes[tstack_p.node].children[child]

    if node == CONTENTS_SOLID:
      return false

    tnode = addr(tnodes[node])

    case tnode.nodetype
    of PLANE_X:
      front = frontx - tnode.dist
      back  = backx -  tnode.dist
    of PLANE_Y:
      front = fronty - tnode.dist
      back  = backy -  tnode.dist
    of PLANE_Z:
      front = frontz - tnode.dist
      back  = backz -  tnode.dist
    else:
      front = (frontx * tnode.normal[0] + fronty * tnode.normal[1] +
               frontz * tnode.normal[2]) - tnode.dist
      back = (backx * tnode.normal[0] + backy * tnode.normal[1] +
              backz * tnode.normal[2]) - tnode.dist

    if front > -ON_EPSILON and back > -ON_EPSILON:
      node = tnode.children[0]
      continue

    if front < ON_EPSILON and back < ON_EPSILON:
      node = tnode.children[1]
      continue

    if front < 0: side = 1 # side = front < 0
    else: side = 0

    front = front / (front - back)
    tstack_p.node = node
    tstack_p.side = side
    tstack_p.backpt[0] = backx
    tstack_p.backpt[1] = backy
    tstack_p.backpt[2] = backz

    tstack_p += 1

    backx = frontx + front * (backx - frontx)
    backy = fronty + front * (backy - fronty)
    backz = frontz + front * (backz - frontz)

    node = tnode.children[side]


proc TestLineNoPtrs*(start: Vec3; stop: Vec3): bool =
  ## LINE TRACING:
  ## The major lighting operation is a point to point visibility test,
  ## performed recursive subdivision of the line by the BSP tree.
  var node, side: int32
  var front, back: float32
  var front3, back3: Vec3
  var tracestack: array[64, Tracestack]
  var tnode: Tnode
  var tsidx: int32 = 0

  front3 = start
  back3 = stop
  node = 0

  while true:
    while node < 0 and node != CONTENTS_SOLID:

      tsidx -= 1

      if tsidx < 0:
        return true

      front3 = back3                     #  set the hit point for this plane
      back3 = tracestack[tsidx].backpt   #  go down the back side

      let child = (not tracestack[tsidx].side) + 2
      node = tnodes[tracestack[tsidx].node].children[child]

    if node == CONTENTS_SOLID:
      return false

    tnode = tnodes[node]

    case tnode.nodetype:
    of PLANE_X:
      front = front3[0] - tnode.dist
      back = back3[0] - tnode.dist
    of PLANE_Y:
      front = front3[1] - tnode.dist
      back = back3[1] - tnode.dist
    of PLANE_Z:
      front = front3[2] - tnode.dist
      back = back3[2] - tnode.dist
    else:
      front = (front3[0] * tnode.normal[0] +
               front3[1] * tnode.normal[1] +
               front3[2] * tnode.normal[2]) - tnode.dist

      back = (back3[0] * tnode.normal[0] +
              back3[1] * tnode.normal[1] +
              back3[2] * tnode.normal[2]) - tnode.dist


    if front > -ON_EPSILON and back > -ON_EPSILON:
      node = tnode.children[0]
      continue

    if front < ON_EPSILON and back < ON_EPSILON:
      node = tnode.children[1]
      continue

    if front < 0: side = 1 else: side = 0

    front = front / (front - back)
    tracestack[tsidx].node = node
    tracestack[tsidx].side = side
    tracestack[tsidx].backpt = back3

    tsidx += 1

    back3[0] = front3[0] + front * (back3[0] - front3[0])
    back3[1] = front3[1] + front * (back3[1] - front3[1])
    back3[2] = front3[2] + front * (back3[2] - front3[2])

    node = tnode.children[side]


