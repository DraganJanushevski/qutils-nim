##  entities.c

import ../COMMON/HEADER_BSPFILE
import ../COMMON/BSPFILE
import strutils


type
  Epair_t* = object # size = 1060
    next*: ptr Epair_t
    key*: array[MAX_KEY, char]
    value*: array[MAX_VALUE, char]

  Entity_t* = object # size = 160
    classname*: array[64, char]
    origin*: array[3, float32] # origin*: Vec3
    angle*: float32
    light*: int32
    style*: int32
    target*: array[32, char]
    targetname*: array[32, char]
    epairs*: ptr Epair_t
    targetent*: ptr Entity_t

var entities*: array[MAX_MAP_ENTITIES, Entity_t]
var num_entities*: int
var numlighttargets*: int32
var lighttargets*: array[64, array[32, char]]

proc LightStyleForTargetname*(targetname: array[32, char]; alloc: bool): int32 =
  ## ENTITY FILE PARSING
  ## If a light has a targetname, generate
  ## unique style in the 32-63 range

  var i: int = 0

  for lt in 0 ..< numlighttargets:
    i = lt
    if lighttargets[lt].join() == targetname.join():
      return 32 + lt.int32
  if not alloc: return -1

  lighttargets[i] = targetname
  inc(numlighttargets)
  return numlighttargets - 1 + 32

proc ValueForKey*(ent: ptr Entity_t; key: array[32, char]): cstring =
  var ep: ptr Epair_t
  ep = ent.epairs
  while ep != nil:
    if ep.key == key:
      return ep.value.join()
    ep = ep.next
  return ""

iterator items(a: ptr Epair_t): ptr Epair_t =
  var x = a
  while x != nil:
    yield x
    x = x.next

proc SetKeyValue*(ent: ptr Entity_t; key: array[32, char],
                                   value: array[1024, char]) =
  for item in ent.epairs:
    if item.key == key:
      item.value = value
      return

  var ep = cast[ptr Epair_t](alloc(sizeof(Epair_t)))
  ep.key = key
  ep.value = value
  ep.next = ent.epairs
  ent.epairs = ep

proc strTo3f(str: string): array[3, float32] =
  for f in 0..2: result[f] = str.split()[f].parseFloat
  result

proc strToCharArr32(str:string): array[32, char] =
  for c in 0 ..< str.len(): result[c] = str[c]
  result

proc strToCharArr1024(str:string): array[1024, char] =
  for c in 0 ..< str.len(): result[c] = str[c]
  result


proc WriteEntitiesToString*() =

  var strline: string = ""
  for i in 0 ..< num_entities:
    strline.add("{\n")
    for ep in entities[i].epairs:
      let k = ep.key.join().split("\0").join()
      let v = ep.value.join().split("\0").join()
      strline.add("\"" & k & "\" " & "\"" & v & "\"" & "\n")
    strline.add("}\n")

  for c in 0 ..< strline.len():
    dentdata[c] = strline[c]

  entdatasize = strline.len().int32 + 1


proc MatchTargets*() =
  var parsedEntites = 0

  for i in 0 ..< num_entities:

    if entities[i].target[0] == '\0':
      continue

    for j in 0 ..< num_entities:
      if entities[j].targetname == entities[i].target:
        entities[i].targetent = entities[j].addr
        parsedEntites = j
        break

    if parsedEntites == num_entities:
      echo("WARNING: entity at ", entities[i].origin, entities[i].classname, " has unmatched target")
      continue

    # set the style on the source ent
    # for switchable lights
    if entities[parsedEntites].style > 0:
      var s: array[1024, char]
      entities[i].style = entities[parsedEntites].style
      s = entities[i].style.intToStr().strToCharArr1024()
      SetKeyValue(entities[i].addr, "style".strToCharArr32(), s)


proc LoadEntities*() =

  let data = dentdata.join().split('\0').join().strip()
  let dsplit = data.split('\n')

  var entity: ptr Entity_t
  var epair:  ptr Epair_t
  var tmpkv: tuple[key: string, value: string]

  num_entities = 0

  for line in dsplit:

    if line == "{":
      entity = entities[num_entities].addr
      continue

    if line == "}":
      inc(num_entities)
      continue

    let sl = line.split()
    tmpkv.key =   sl[0].replace("\"", "")
    tmpkv.value = sl[1..sl.high].join(" ").replace("\"", "")

    ## -- add epairs -- ##
    epair = cast[ptr Epair_t](alloc(sizeof(Epair_t)))

    epair.key = tmpkv.key.strToCharArr32()
    epair.value = tmpkv.value.strToCharArr1024()
    epair.next = entity.epairs
    entity.epairs = epair

    if tmpkv.key == "classname":
      for k in 0 ..< tmpkv.value.len:
        entity.classname[k] = tmpkv.value[k]

    if tmpkv.key == "target":
      entity.target = tmpkv.value.strToCharArr32()

    if tmpkv.key == "targetname":
      entity.targetname = tmpkv.value.strToCharArr32()

    if tmpkv.key == "origin":
      entity.origin = line.replace("\"", "").split()[1..3].join(" ").strTo3f

    if tmpkv.key.startsWith("light"):
      entity.light = tmpkv.value.split[0].parseInt.int32

    if tmpkv.key == "style":
      entity.style = tmpkv.value.parseInt.int32

    if tmpkv.key == "angle":
      entity.angle = tmpkv.value.parseFloat


  for i in 0 ..< num_entities:
    var entity = entities[i].addr
    let clight = ['l','i','g','h','t']

    if entity.classname[0..4] == clight and entity.light == 0:
      entity.light = 300

    if entity.classname[0..4] == clight:
      if entity.targetname[0] != '\0' and entity.style == 0:

        entity.style = LightStyleForTargetname(entity.targetname, true)
        SetKeyValue(entity, "style".strToCharArr32(),
                    entity.style.intToStr().join().strToCharArr1024())

  MatchTargets()

  echo "Entites: ", num_entities
