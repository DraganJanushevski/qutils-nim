# import nimprof
import os

import ../COMMON/BSPFILE
import ENTITIES
import TRACE
import LTFACE

import threadpool


proc LightThread(junk: pointer) =

  for i in 0..numfaces - 1:
    spawn LightFace(i, cntr)


proc LightWorld() =
  lightdatasize = 0
  LightThread(nil)

  var count: int32 = 0
  var fidx : int32 = 0

  for lm in globalLightmaps[0..numfaces-1]:
    let face = dfaces[fidx].addr
    fidx += 1

    if lm.style[0].len > 0:
      face.lightofs = count
      for s in 0..3:
        if lm.style[s].len > 0:
          for b in 0..lm.style[s].len:
            dlightdata[count] = lm.style[s].data[b]
            count += 1


import strutils
import times, os

template benchmark*(benchmarkName: string, code: untyped) =
  block:
    let t0 = epochTime()
    code
    let elapsed = epochTime() - t0
    let elapsedStr = elapsed.formatFloat(format = ffDecimal, precision = 3)
    echo "CPU Time [", benchmarkName, "] ", elapsedStr, "s"

proc main() =

  let argc = paramCount()
  var i: int = 1

  echo("----- LightFaces -----")

  for v in 1..argc:
    if paramStr(i) == "-threads":
      ## numthreads = atoi (argv[i+1]);
      ## i++;
    elif paramStr(i) == "-extra":
      extrasamples = true
      echo("INFO: extra sampling enabled")
      i += 1
    elif paramStr(i) == "-dist":
      scaledist = parsefloat(paramStr(i+1))
      echo "INFO: scaledistance: ", scaledist
      i += 2
    elif paramStr(i) == "-range":
      rangescale = parsefloat(paramStr(i+1))
      echo "INFO: rangescale:    ", rangescale
      i += 2
    elif paramStr(i) == "-":
      quit("ERROR: Unknown option: - ")
    else:
      break

  if i != argc:
    quit("usage: light [-threads num] [-extra] bspfile")

  echo("LOADING ", paramStr(i), " ...")

  LoadBSPFile(paramStr(i))
  benchmark("LoadEntities ", LoadEntities() )
  benchmark("MakeTnodes   ", MakeTnodes(dmodels[0].addr) )
  benchmark("LightWorld   ", LightWorld() )
  benchmark("WriteEntities", WriteEntitiesToString() )
  echo("Mem footprint: ", (getTotalMem()/(1024*1024)).formatFloat(ffDecimal, 3), " [MB]")

  WriteBSPFile(paramStr(i).split(".")[0] & "NIM" & "." & paramStr(i).split(".")[1])

main()
