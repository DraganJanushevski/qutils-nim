import ../COMMON/HEADER_BSPFILE
import ../COMMON/BSPFILE
import ../COMMON/MATHLIB
import ../COMMON/MATHLIBH
import ../COMMON/ptrmath
import ENTITIES
from TRACE import TestLine, TestLineNoPtrs
import math


const SINGLEMAP* = (18 * 18 * 4)
var scaledist*: float32 = 1.0'f32
var scalecos*: float32 = 0.5'f32
var rangescale*: float32 = 0.5'f32
var extrasamples*: bool
var minlights*: array[MAX_MAP_FACES, float32]

type
  lightinfo_t* = object
    lightmaps*: array[MAXLIGHTMAPS, array[SINGLEMAP, Vec1]]
    numlightstyles*: int32
    light*: ptr Vec1
    facedist*: Vec1
    facenormal*: Vec3
    numsurfpt*: int32
    surfpt*: array[SINGLEMAP, Vec3]
    texorg*: Vec3
    worldtotex*: array[2, Vec3] ##  s = (world - texorg) . worldtotex[0]
    textoworld*: array[2, Vec3] ##  world = texorg + s * textoworld[0]
    exactmins*: array[2, Vec1]
    exactmaxs*: array[2, Vec1]
    texmins*: array[2, int32]
    texsize*: array[2, int32]
    lightstyles*: array[256, int32]
    surfnum*: int32
    face*: ptr dface_t

proc CastRay(p1: Vec3; p2: Vec3): Vec1 =
  ## Returns the distance between the points,
  ## or -1 if blocked
  if not TestLine(p1, p2):
    return -1

  var t: Vec1 = 0
  for i in 0..2:
    t += (p2[i]-p1[i]) * (p2[i]-p1[i])

  if t == 0: t = 1
  return sqrt(t)

proc CalcFaceVectors(l: ptr lightinfo_t) =
  ##  Fills in texorg, worldtotex. and textoworld
  let tex = texinfo[l.face.texinfo].addr
  for i in 0..1:
    for j in 0..2:
      l.worldtotex[i][j] = tex.vecs[i][j]

  ##  calculate a normal to the texture axis
  ##  points can be moved along this without changing their S/T
  var texnormal: Vec3
  texnormal[0] = tex.vecs[1][1] * tex.vecs[0][2] - tex.vecs[1][2] * tex.vecs[0][1]
  texnormal[1] = tex.vecs[1][2] * tex.vecs[0][0] - tex.vecs[1][0] * tex.vecs[0][2]
  texnormal[2] = tex.vecs[1][0] * tex.vecs[0][1] - tex.vecs[1][1] * tex.vecs[0][0]
  texnormal = VectorNormalize(texnormal)

  ##  flip it towards plane normal
  var distscale = DotProduct(texnormal, l.facenormal)
  if distscale == 0:
    echo("Texture axis perpendicular to face")
  if distscale < 0:
    distscale = -distscale
    VectorSubtract(vec3_origin, texnormal, texnormal)
  distscale = 1 / distscale

  var dist, len: Vec1
  for i in 0..1:
    len = VectorLength(l.worldtotex[i])
    dist = DotProduct(l.worldtotex[i], l.facenormal)
    dist *= distscale
    VectorMA(l.worldtotex[i], -dist, texnormal, l.textoworld[i])
    VectorScale(l.textoworld[i], (1 / len) * (1 / len), l.textoworld[i])



  ##  calculate texorg on the texture plane
  for i in 0..2:
    l.texorg[i] = -(tex.vecs[0][3] * l.textoworld[0][i]) -
                    tex.vecs[1][3] * l.textoworld[1][i]

  ##  project back to the face plane
  dist = DotProduct(l.texorg, l.facenormal) - l.facedist - 1
  dist *= distscale
  VectorMA(l.texorg, -dist, texnormal, l.texorg)


proc CalcFaceExtents(l: ptr lightinfo_t) =
  ## Fill in s->texmins[] and s->texsize[]
  ## also sets exactmins[] and exactmaxs[]

  var mins = [999999'f32, 999999]
  var maxs = [-99999'f32, -99999]
  var vertex: ptr dvertex_t
  let face = l.face
  let tex =  texinfo[face.texinfo].addr

  for i in 0..face.numedges-1:
    let edge = dsurfedges[face.firstedge + i]
    if edge >= 0:
      vertex = dvertexes[dedges[ edge].v[0]].addr
    else:
      vertex = dvertexes[dedges[-edge].v[1]].addr

    for j in 0..1:
      let val = vertex.point[0] * tex.vecs[j][0] +
                vertex.point[1] * tex.vecs[j][1] +
                vertex.point[2] * tex.vecs[j][2] + tex.vecs[j][3]
      if val < mins[j]: mins[j] = val
      if val > maxs[j]: maxs[j] = val

  for i in 0..1:
    l.exactmins[i] = mins[i]
    l.exactmaxs[i] = maxs[i]
    mins[i] = floor(mins[i] / 16)
    maxs[i] =  ceil(maxs[i] / 16)
    l.texmins[i] = mins[i].int32
    l.texsize[i] = (maxs[i] - mins[i]).int32

    if l.texsize[i] > 17:
      echo("Bad surface extents ", l.texsize)


proc CalcPoints(l: ptr lightinfo_t) =
  ## For each texture aligned grid point, back project onto the plane
  ## to get the world xyz value of the sample point
  ##
  ##  fill in surforg
  ##  the points are biased towards the center of the surface
  ##  to help avoid edge cases just inside walls
  var starts, startt, us, ut: Vec1
  var facemid, move: Vec3
  var w, h, step: int32
  if extrasamples: ##  extra filtering
    h = (l.texsize[1] + 1) * 2
    w = (l.texsize[0] + 1) * 2
    starts = (l.texmins[0].float32 - 0.5'f32) * 16
    startt = (l.texmins[1].float32 - 0.5'f32) * 16
    step = 8
  else:
    h = l.texsize[1] + 1
    w = l.texsize[0] + 1
    starts = l.texmins[0].float32 * 16
    startt = l.texmins[1].float32 * 16
    step = 16
  l.numsurfpt = w * h

  let mids = (l.exactmaxs[0] + l.exactmins[0]) / 2
  let midt = (l.exactmaxs[1] + l.exactmins[1]) / 2

  for j in 0..2:
    facemid[j] = l.texorg[j] + l.textoworld[0][j] * mids +
                               l.textoworld[1][j] * midt

  var sptidx = 0
  for t in 0 ..< h:
    for s in 0 ..< w:

      var surf = l.surfpt[sptidx].addr
      us = starts + (s*step).float32
      ut = startt + (t*step).float32

      ##  if a line can be traced from surf to facemid, the point is good
      for i in 0..5:
        ##  calculate texture point
        for j in 0..2:
          surf[j] = l.texorg[j] + l.textoworld[0][j] * us +
                                  l.textoworld[1][j] * ut

        if CastRay(facemid, surf[]) != -1:
          break

        if (i and 1) > 0:
          if us > mids:
            us -= 8
            if us < mids: us = mids
          else:
            us += 8
            if us > mids: us = mids
        else:
          if ut > midt:
            ut -= 8
            if ut < midt: ut = midt
          else:
            ut += 8
            if ut > midt: ut = midt

        ## move surf 8 pixels towards the center
        VectorSubtract(facemid, surf[], move)
        move = VectorNormalize(move)
        VectorMA(surf[], 8, move, surf[])

      sptidx += 1


# var c_culldistplane*, c_proper*: int32

proc SingleLightFace(light: ptr Entity_t; l: ptr lightinfo_t) =

  var dist: Vec1
  var rel: Vec3

  VectorSubtract(light.origin, [0.0'f32, 0.0, 0.0], rel)
  dist = scaledist * (DotProduct(rel, l.facenormal) - l.facedist)
  ##  don't bother with lights behind the surface
  if dist <= 0:
    return
  if dist > light.light.float32:
    return

  var spotvec: Vec3
  var falloff: Vec1 = 0
  if light.targetent != nil:
    VectorSubtract(light.targetent.origin, light.origin, spotvec)
    spotvec = VectorNormalize(spotvec)
    if light.angle == 0:
      falloff = -cos(20 * Q_PI / 180)
    else:
      falloff = -cos(light.angle / 2 * Q_PI / 180)

  var mapnum = 0
  while mapnum < l.numlightstyles:
    if l.lightstyles[mapnum] == light.style:
      break
    inc(mapnum)

  var lightsamp = l.lightmaps[mapnum][0].addr

  if mapnum == l.numlightstyles:
    # init a new light map
    if mapnum == MAXLIGHTMAPS:
      echo("WARNING: Too many light styles on a face")
      return

  var add: Vec1
  var hit = false

  for c in 0..l.numsurfpt-1:

    let surf3p = l.surfpt[c];

    dist = CastRay(light.origin, surf3p) * scaledist
    if dist < 0:
      continue # light doesn't reach

    var incoming: Vec3
    VectorSubtract(light.origin, surf3p, incoming)
    incoming = VectorNormalize(incoming)

    var angle = DotProduct(incoming, l.facenormal)
    if light.targetent != nil: ##  spotlight cutoff
      if DotProduct(spotvec, incoming) > falloff:
        continue

    angle = (1.0 - scalecos) + scalecos * angle
    add = light.light.float32 - dist
    add *= angle
    if add < 0:
      continue

    lightsamp[c] += add
    if lightsamp[c] > 1:
      hit = true # ignore real tiny lights

  if mapnum == l.numlightstyles and hit == true:
    l.lightstyles[mapnum] = light.style
    l.numlightstyles += 1 # the style has some real data now


proc FixMinlight(l: ptr lightinfo_t) =
  ##  if minlight is set, there must be a style 0 light map
  var minlight = minlights[l.surfnum]
  if minlight == 0:
    return

  var i: int32
  for i in 0..l.numlightstyles-1:
    if l.lightstyles[i] == 0:
      break

  if i == l.numlightstyles:
    if l.numlightstyles == MAXLIGHTMAPS:
      return # oh well..

    for j in 0..l.numsurfpt-1:
      l.lightmaps[i][j] = minlight

    l.lightstyles[i] = 0
    inc(l.numlightstyles)
  else:
    for j in 0..l.numsurfpt-1:
      if l.lightmaps[i][j] < minlight:
         l.lightmaps[i][j] = minlight


proc LightFace*(surfnum: int32; writepos: int32) =
  var l: lightinfo_t
  var f = dfaces[surfnum].addr

  f.lightofs = -1  ##  some surfaces don't need lightmaps

  for j in 0 ..< MAXLIGHTMAPS:
    f.styles[j] = 255

  if texinfo[f.texinfo].flags > 0 and TEX_SPECIAL.bool:
    return ##  non-lit texture

  zeroMem(l.addr, sizeof(l))
  l.surfnum = surfnum
  l.face = f

  ##  rotate plane
  l.facenormal = dplanes[f.planenum].normal
  l.facedist =   dplanes[f.planenum].dist

  if f.side > 0:
    VectorSubtract(vec3_origin, l.facenormal, l.facenormal)
    l.facedist = -l.facedist

  CalcFaceVectors(addr(l))
  CalcFaceExtents(addr(l))
  CalcPoints(addr(l))

  var lightmapwidth = l.texsize[0] + 1
  var size = lightmapwidth * (l.texsize[1] + 1)

  if size > SINGLEMAP:
    echo("Bad lightmap size ", size)
  if lightmapwidth == 0:
    echo("Zero lightmap size")

  for i in 0 ..< MAXLIGHTMAPS:
    l.lightstyles[i] = 255

  ##  cast all lights ##
  l.numlightstyles = 0

  for i in 0 ..< num_entities:
    if entities[i].light > 0:
      SingleLightFace(addr(entities[i]), addr(l))

  FixMinlight(addr(l))
  if l.numlightstyles == 0:
    return

  for i in 0 ..< MAXLIGHTMAPS:
    f.styles[i] = l.lightstyles[i].byte

  var lightmapsize = size * l.numlightstyles
  lightdatasize += (lightmapsize + 3) and not 3 # GetFileSpaceInt()

  # extra filtering
  # var h = (l.texsize[1] + 1) * 2
  let w = (l.texsize[0] + 1) * 2

  var total: float32
  var LM = globalLightmaps[surfnum].addr
  LM.offset = cntr

  for i in 0 ..< l.numlightstyles:
    if l.lightstyles[i] == 0x000000FF:
      echo("Wrote empty lightmap")

    var light = l.lightmaps[i][0].addr
    var c: int32 = 0

    for t in 0 .. l.texsize[1]:
      for s in 0 .. l.texsize[0]:

        if extrasamples: # filtered sample
          total = light[t * 2 * w + s * 2] +
                  light[t * 2 * w + s * 2 + 1] +
                  light[(t * 2 + 1) * w + s * 2] +
                  light[(t * 2 + 1) * w + s * 2 + 1]
          total = total * 0.25'f32
        else:
          total = light[c]

        total *= rangescale # scale before clamping
        total = total.clamp(0, 255)

        LM.style[i].data[c] = total.uint8

        inc(cntr)
        inc(c)

    LM.style[i].len = c-1
