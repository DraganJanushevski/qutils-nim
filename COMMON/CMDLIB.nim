##  cmdlib.c


proc SafeOpenWrite*(filename: string): FILE =
  var f: FILE
  discard open(f, filename, fmWrite)
  if f == nil:
    echo("Error opening ", filename)
  return f

proc SafeOpenRead*(filename: string): ptr FILE =
  var f: FILE
  discard open(f, filename, fmRead)
  if f == nil:
    echo("Error opening ", filename)
  return f.addr

proc SafeRead*(f: ptr FILE; buffer: pointer; count: int) =
  # if fread(buffer, 1, count, f) != cast[csize](count):
  if readBuffer(f[], buffer, count) != count:
    echo("File read failure")

proc SafeWrite*(f: ptr FILE; buffer: pointer; count: int) =
  if writeBuffer(f[], buffer, count) != count:
    echo("File write failure")


proc LoadFile*(filename: string): FILE =
  var length: int64
  let f = open(filename)
  length = getFileSize(f)
  return f
