##  upper design bounds
const
  MAX_MAP_HULLS* = 4
  MAX_MAP_MODELS* = 256
  MAX_MAP_BRUSHES* = 4096
  MAX_MAP_ENTITIES* = 1024
  MAX_MAP_ENTSTRING* = 65536
  MAX_MAP_PLANES* = 8192
  MAX_MAP_NODES* = 32767
  MAX_MAP_CLIPNODES* = 32767
  MAX_MAP_LEAFS* = 32767
  MAX_MAP_VERTS* = 65535
  MAX_MAP_FACES* = 65535
  MAX_MAP_MARKSURFACES* = 65535
  MAX_MAP_TEXINFO* = 4096
  MAX_MAP_EDGES* = 256000
  MAX_MAP_SURFEDGES* = 512000
  MAX_MAP_MIPTEX* = 0x00200000
  MAX_MAP_LIGHTING* = 0x00100000
  MAX_MAP_VISIBILITY* = 0x00100000

const # key / value pair sizes
  MAX_KEY* = 32
  MAX_VALUE* = 1024

const
  BSPVERSION* = 29

const
  LUMP_ENTITIES* = 0
  LUMP_PLANES* = 1
  LUMP_TEXTURES* = 2
  LUMP_VERTEXES* = 3
  LUMP_VISIBILITY* = 4
  LUMP_NODES* = 5
  LUMP_TEXINFO* = 6
  LUMP_FACES* = 7
  LUMP_LIGHTING* = 8
  LUMP_CLIPNODES* = 9
  LUMP_LEAFS* = 10
  LUMP_MARKSURFACES* = 11
  LUMP_EDGES* = 12
  LUMP_SURFEDGES* = 13
  LUMP_MODELS* = 14
  HEADER_LUMPS* = 15

type
  lump_t* = object
    fileofs*: int32
    filelen*: int32

type
  dmodel_t* = object
    mins*: array[3, cfloat]
    maxs*: array[3, cfloat]
    origin*: array[3, cfloat]
    headnode*: array[MAX_MAP_HULLS, cint]
    visleafs*: cint            ##  not including the solid leaf 0
    firstface*: cint
    numfaces*: cint

  dheader_t* = object
    version*: int32
    lumps*: array[HEADER_LUMPS, lump_t]

  dmiptexlump_t* = object
    nummiptex*: int32
    dataofs*: array[4, int32]    ##  [nummiptex]


const
  MIPLEVELS* = 4

type
  miptex_t* = object
    name*: array[16, char]
    width*: cuint
    height*: cuint
    offsets*: array[MIPLEVELS, cuint] ##  four mip maps stored

  dvertex_t* = object
    point*: array[3, float32]

const
  PLANE_X* = 0    ##  0-2 are axial planes
  PLANE_Y* = 1
  PLANE_Z* = 2
  PLANE_ANYX* = 3 ##  3-5 are non-axial planes snapped to the nearest
  PLANE_ANYY* = 4
  PLANE_ANYZ* = 5

type
  Dplane* = object
    normal*: array[3, float32]
    dist*: float32
    planetype*: int32  ##  PLANE_X - PLANE_ANYZ ?remove? trivial to regenerate

const
  CONTENTS_EMPTY* = -1
  CONTENTS_SOLID* = -2
  CONTENTS_WATER* = -3
  CONTENTS_SLIME* = -4
  CONTENTS_LAVA* = -5
  CONTENTS_SKY* = -6

##  !!! if this is changed, it must be changed in asm_i386.h too !!!
type
  Dnode* = object
    planenum*: int32
    children*: array[2, int16] ##  negative numbers are -(leafs+1), not nodes
    mins*: array[3, int16]     ##  for sphere culling
    maxs*: array[3, int16]
    firstface*: uint16
    numfaces*: uint16         ##  counting both sides

  dclipnode_t* = object
    planenum*: int32
    children*: array[2, uint16] ##  negative numbers are contents

  texinfo_t* = object
    vecs*: array[2, array[4, float32]] ##  [s/t][xyz offset]
    miptex*: int32
    flags*: int32

const
  TEX_SPECIAL* = 1

##  note that edge 0 is never used, because negative edge nums are used for
##  counterclockwise use of the edge in a face
type
  dedge_t* = object
    v*: array[2, uint16]       ##  vertex numbers

const
  MAXLIGHTMAPS* = 4

type
  dface_t* = object
    planenum*: int16
    side*: int16
    firstedge*: int32   ##  we must support > 64k edges
    numedges*: int16
    texinfo*: int16 ##  lighting info
    styles*: array[MAXLIGHTMAPS, byte]
    lightofs*: int32 ##  start of [numstyles*surfsize] samples

const
  AMBIENT_WATER* = 0
  AMBIENT_SKY* = 1
  AMBIENT_SLIME* = 2
  AMBIENT_LAVA* = 3
  NUM_AMBIENTS* = 4

##  leaf 0 is the generic CONTENTS_SOLID leaf, used for all solid areas
##  all other leafs need visibility info

type
  dleaf_t* = object
    contents*: cint
    visofs*: cint               ## -1 = no visibility info
    mins*: array[3, cshort]     ##  for frustum culling
    maxs*: array[3, cshort]
    firstmarksurface*: cushort
    nummarksurfaces*: cushort
    ambient_level*: array[NUM_AMBIENTS, byte]
