##  mathlib.h

when defined(DOUBLEVEC_T):
  type
    Vec1* = float64
else:
  type
    Vec1* = float32

type
  Vec3* = array[3, Vec1]

const
  SIDE_FRONT* = 0
  SIDE_ON* = 2
  SIDE_BACK* = 1
  SIDE_CROSS* = -2
  Q_PI* = 3.141592653589793'f32

var vec3_origin*: Vec3

const
  EQUAL_EPSILON* = 0.001'f32


proc DotProduct*(x, y: Vec3): float32 =
  (x[0] * y[0] + x[1] * y[1] + x[2] * y[2])

proc VectorSubtract*(a, b: Vec3, c: var Vec3): void =
  c[0] = a[0] - b[0]
  c[1] = a[1] - b[1]
  c[2] = a[2] - b[2]

proc VectorAdd*(a, b: Vec3, c: var Vec3): void =
  c[0] = a[0] + b[0]
  c[1] = a[1] + b[1]
  c[2] = a[2] + b[2]

proc VectorCopy*(a: Vec3, b: var Vec3): void =
  b[0] = a[0]
  b[1] = a[1]
  b[2] = a[2]
