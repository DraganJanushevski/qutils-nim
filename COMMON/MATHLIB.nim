##  mathlib.c -- math primitives
import math
import MATHLIBH

# var vec3_origin*: Vec3 = [0.0'f32, 0.0, 0.0] ## clash in mathlib.h

proc VectorLength*(v: Vec3): float64 =
  var length: float64 = 0
  for i in 0..2: length += v[i]*v[i]
  sqrt(length)

proc VectorCompare*(v1: Vec3; v2: Vec3): bool =
  for i in 0..2:
    if abs(v1[i] - v2[i]) > EQUAL_EPSILON:
      return false
  return true

proc Q_rint*(i: Vec1): Vec1 =
  return floor(i + 0.5)

proc VectorMA*(va: Vec3; scale: float32; vb: Vec3; vc: var Vec3) =
  vc[0] = va[0] + scale * vb[0]
  vc[1] = va[1] + scale * vb[1]
  vc[2] = va[2] + scale * vb[2]

proc CrossProduct*(v1: Vec3; v2: Vec3; cross: var Vec3) =
  cross[0] = v1[1] * v2[2] - v1[2] * v2[1]
  cross[1] = v1[2] * v2[0] - v1[0] * v2[2]
  cross[2] = v1[0] * v2[1] - v1[1] * v2[0]

proc procDotProduct*(v1: Vec3; v2: Vec3): Vec1 =
  return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2]

proc procVectorSubtract*(va: Vec3; vb: Vec3; o: var Vec3) =
  o[0] = va[0] - vb[0]
  o[1] = va[1] - vb[1]
  o[2] = va[2] - vb[2]

proc procVectorAdd*(va: Vec3; vb: Vec3; o: var Vec3) =
  o[0] = va[0] + vb[0]
  o[1] = va[1] + vb[1]
  o[2] = va[2] + vb[2]

proc procVectorCopy*(i: Vec3; o: var Vec3) =
  o[0] = i[0]
  o[1] = i[1]
  o[2] = i[2]


proc VectorNormalize*(v: Vec3): Vec3 =
  var length: float32 = 0

  for i in 0..2: length += v[i]*v[i]
  length = sqrt(length)

  var o = [0.0'f32, 0.0, 0.0]
  if length == 0:
    return o

  for i in 0..2: o[i] = v[i] / length

  return o

proc VectorInverse*(v: var Vec3) =
  v[0] = -v[0]
  v[1] = -v[1]
  v[2] = -v[2]

proc VectorScale*(v: Vec3; scale: Vec1; o: var Vec3) =
  o[0] = v[0] * scale
  o[1] = v[1] * scale
  o[2] = v[2] * scale
