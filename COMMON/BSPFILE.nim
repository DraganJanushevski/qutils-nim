import
  CMDLIB,
  HEADER_BSPFILE


var cntr*: int32 = 0

type
  lmbits* = object
    data*: array[1296, uint8]
    len*: int32

  Lightmap* = object
    offset*: int32
    style*: array[4, lmbits]

var globalLightmaps*: array[MAX_MAP_FACES, Lightmap]

var nummodels*: int32
var dmodels*: array[MAX_MAP_MODELS, dmodel_t]

var visdatasize*: int32
var dvisdata*: array[MAX_MAP_VISIBILITY, byte]

var lightdatasize*: int32
var dlightdata*: array[MAX_MAP_LIGHTING, byte]

var texdatasize*: int32
var dtexdata*: array[MAX_MAP_MIPTEX, byte] ##  (dmiptexlump_t)

var entdatasize*: int32
var dentdata*: array[MAX_MAP_ENTSTRING, char]

var numleafs*: int32
var dleafs*: array[MAX_MAP_LEAFS, dleaf_t]

var numplanes*: int32
var dplanes*: array[MAX_MAP_PLANES, Dplane]

var numvertexes*: int32
var dvertexes*: array[MAX_MAP_VERTS, dvertex_t]

var numnodes*: int32
var dnodes*: array[MAX_MAP_NODES, Dnode]

var numtexinfo*: int32
var texinfo*: array[MAX_MAP_TEXINFO, texinfo_t]

var numfaces*: int32
var dfaces*: array[MAX_MAP_FACES, dface_t]

var numclipnodes*: int32
var dclipnodes*: array[MAX_MAP_CLIPNODES, dclipnode_t]

var numedges*: int32
var dedges*: array[MAX_MAP_EDGES, dedge_t]

var nummarksurfaces*: int32
var dmarksurfaces*: array[MAX_MAP_MARKSURFACES, uint8]

var numsurfedges*: int32
var dsurfedges*: array[MAX_MAP_SURFEDGES, int32]


var header*: dheader_t ##OK

proc CopyLump*(f: FILE, lump:int, dest:pointer, size:int): int =

  let length = header.lumps[lump].filelen
  let ofs = header.lumps[lump].fileofs

  if length mod size > 0:
    echo("LoadBSPFile: odd lump size")

  let numlumpitems = length div size

  setFilePos(f, ofs.clong)
  discard f.readBuffer(dest, numlumpitems * size)

  return numlumpitems



proc LoadBSPFile*(filename: string) =

  let f = LoadFile(filename)

  discard f.readBuffer(header.addr, sizeof(dheader_t))

  echo "BSP Version: ", header.version

  nummodels =       CopyLump(f, LUMP_MODELS, dmodels.addr, sizeof(dmodel_t)).int32
  numvertexes =     CopyLump(f, LUMP_VERTEXES, dvertexes.addr, sizeof(dvertex_t)).int32
  numplanes =       CopyLump(f, LUMP_PLANES, dplanes.addr, sizeof(Dplane)).int32
  numleafs =        CopyLump(f, LUMP_LEAFS, dleafs.addr, sizeof(dleaf_t)).int32
  numnodes =        CopyLump(f, LUMP_NODES, dnodes.addr, sizeof(Dnode)).int32
  numtexinfo =      CopyLump(f, LUMP_TEXINFO, texinfo.addr, sizeof(texinfo_t)).int32
  numclipnodes =    CopyLump(f, LUMP_CLIPNODES, dclipnodes.addr, sizeof(dclipnode_t)).int32
  numfaces =        CopyLump(f, LUMP_FACES, dfaces.addr, sizeof(dface_t)).int32
  nummarksurfaces = CopyLump(f, LUMP_MARKSURFACES, dmarksurfaces.addr, sizeof(dmarksurfaces[0])).int32
  numsurfedges =    CopyLump(f, LUMP_SURFEDGES, dsurfedges.addr, sizeof(dsurfedges[0])).int32
  numedges =        CopyLump(f, LUMP_EDGES, dedges.addr, sizeof(dedge_t)).int32
  texdatasize =     CopyLump(f, LUMP_TEXTURES, dtexdata.addr, 1).int32
  visdatasize =     CopyLump(f, LUMP_VISIBILITY, dvisdata.addr, 1).int32
  lightdatasize =   CopyLump(f, LUMP_LIGHTING, dlightdata.addr, 1).int32
  entdatasize =     CopyLump(f, LUMP_ENTITIES, dentdata.addr, 1).int32


var wadfile*: FILE
var outheader*: dheader_t

proc AddLump*(lumpnum: int32; data: pointer; len: int32) =

  var lump = addr(header.lumps[lumpnum])
  lump.fileofs = getFilePos(wadfile).int32
  lump.filelen = len

  SafeWrite(wadfile.addr, data, (len + 3) and not 3)


proc WriteBSPFile*(filename: string) =

  echo "Writing: ", filename

  header = outheader
  zeroMem(header.addr, sizeof((dheader_t)))
  header.version = BSPVERSION
  wadfile = SafeOpenWrite(filename)

  SafeWrite(wadfile.addr, header.addr, sizeof(dheader_t))

  AddLump(LUMP_PLANES, dplanes.addr, (numplanes * sizeof(Dplane)).int32)
  AddLump(LUMP_LEAFS, dleafs.addr, numleafs * sizeof(dleaf_t).int32)
  AddLump(LUMP_VERTEXES, dvertexes.addr, numvertexes * sizeof(dvertex_t).int32)
  AddLump(LUMP_NODES, dnodes.addr, numnodes * sizeof(Dnode).int32)
  AddLump(LUMP_TEXINFO, texinfo.addr, numtexinfo * sizeof(texinfo_t).int32)
  AddLump(LUMP_FACES, dfaces.addr, numfaces * sizeof(dface_t).int32)
  AddLump(LUMP_CLIPNODES, dclipnodes.addr, numclipnodes * sizeof(dclipnode_t).int32)
  AddLump(LUMP_MARKSURFACES, dmarksurfaces.addr, nummarksurfaces * sizeof(dmarksurfaces[0]).int32)
  AddLump(LUMP_SURFEDGES, dsurfedges.addr, numsurfedges * sizeof(dsurfedges[0]).int32)
  AddLump(LUMP_EDGES, dedges.addr, numedges * sizeof(dedge_t).int32)
  AddLump(LUMP_MODELS, dmodels.addr, nummodels * sizeof(dmodel_t).int32)
  AddLump(LUMP_LIGHTING, dlightdata.addr, lightdatasize)
  AddLump(LUMP_VISIBILITY, dvisdata.addr, visdatasize)
  AddLump(LUMP_ENTITIES, dentdata.addr, entdatasize)
  AddLump(LUMP_TEXTURES, dtexdata.addr, texdatasize)

  echo "lightdatasize ", lightdatasize

  setFilePos(wadfile, 0)
  SafeWrite(wadfile.addr, header.addr, sizeof(dheader_t))
  close(wadfile)


proc PrintBSPFileSizes*() =
  ## Dumps info
  ## about current file

  echo("planes       ", numplanes,       "   ", numplanes *       sizeof(Dplane))
  echo("vertexes     ", numvertexes,     "   ", numvertexes *     sizeof(dvertex_t))
  echo("nodes        ", numnodes,        "   ", numnodes *        sizeof(Dnode))
  echo("texinfo      ", numtexinfo,      "   ", numtexinfo *      sizeof(texinfo_t))
  echo("faces        ", numfaces,        "   ", numfaces *        sizeof(dface_t))
  echo("clipnodes    ", numclipnodes,    "   ", numclipnodes *    sizeof(dclipnode_t))
  echo("leafs        ", numleafs,        "   ", numleafs *        sizeof(dleaf_t))
  echo("marksurfaces ", nummarksurfaces, "   ", nummarksurfaces * sizeof(dmarksurfaces[0]))
  echo("surfedges    ", numsurfedges,    "   ", numsurfedges *    sizeof(dmarksurfaces[0]))
  echo("edges        ", numedges,        "   ", numedges *        sizeof(dedge_t))

  if texdatasize == 0:
    echo("    0 textures")
  else:
    echo("textures   ", cast[ptr dmiptexlump_t](dtexdata.addr).nummiptex, "   ",  texdatasize )

  echo("      lightdatasize  ", lightdatasize)
  echo("      visdatasize    ", visdatasize)
  echo("      entdatasize    ", entdatasize)
