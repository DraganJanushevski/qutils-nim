# nLight

Nim port of the Quake 1 Light tool

## Usage
- Install [Nim](https://nim-lang.org/)
- Clone this repo and navigate to the root folder
- Compile and Run with
```sh
nim c -r -d:release -d:danger --gc:markandsweep --passL:-s --threads:on LIGHT.nim path/to/your/mapname.bsp
```


![](https://i.postimg.cc/1R05F6nD/spasm-nlight.png)
