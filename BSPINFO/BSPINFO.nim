import os
import ../COMMON/BSPFILE

proc main() =
  var source: string
  let argc = paramCount()

  if argc == 0:
    echo("usage: bspinfo bspfile [bspfiles]")

  for i in 1 .. argc:
    echo("---------------------")
    source = paramStr(i)
    echo("MAP: ", source)
    LoadBSPFile(source)
    PrintBSPFileSizes()
    echo("---------------------")

main()

##! nim c -d:danger --newruntime -d:useMalloc --opt:size --passl:-s -r BSPINFO.nim 00_Q-test_box.bsp
##! nim c -d:danger --newruntime --opt:size --passl:-s -r BSPINFO.nim 00_Q-test_box.bsp